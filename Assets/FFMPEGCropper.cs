﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;

public class FFMPEGCropper : MonoBehaviour
{
    public static string FFMPEGPath {
        get {
            return streamingAssetsPath + @"/ffmpeg-20190504-a5387f9-win64-static/bin/ffmpeg.exe";
        }
    }

    static string streamingAssetsPath;
    static string datapath;

    private void Awake() {
        streamingAssetsPath = Application.streamingAssetsPath;
        datapath = Application.dataPath;        
    }

    public GameObject inProgressPanel;
    public Image progressBar;

    public Text[] lastLogLineVizes;

    public GameObject errorPanel;

    public GameObject doneBox;

    [ContextMenu("TestFast")]
    public void TestFast() {
        RunCrop(@"W:\SHADOWPLAY\Desktop\Desktop 2019.08.30 - 11.02.06.20.DVR.mp4", new CropParams { cropHeight = 500, cropOriginX = 0, cropOriginY = 0, cropWidth = 500 }, true, 10f);
    }

    public void RunCrop(string pathToFile, CropParams cropParams, bool forceEightMeg, float vidLen) {
        SetVizStart();

        var oldF = new FileInfo(pathToFile);
        string outPath = "";
        int it = 0;
        while (true) {
            outPath = Path.Combine(oldF.DirectoryName, oldF.Name.Substring(0, oldF.Name.Length - oldF.Extension.Length) + "_cropped" + (it != 0 ? it.ToString() : "") + oldF.Extension);
            if (!File.Exists(outPath))break;
            it++;
        }



        /*string forceEightMegStr = "";
        if (forceEightMeg) {
            var targFileSize = 1024 * 1024 * 8;
            targFileSize -= 1024 * 1200;

            var calcTargetBitrate = targFileSize / vidLen;
            forceEightMegStr = "-b:v " + ((calcTargetBitrate / 1024f) * 8)+"k";
        }


        string args;
        string passLogFileLoc = null;
        if(forceEightMeg) {
            passLogFileLoc = Path.Combine(new FileInfo(pathToFile).Directory.FullName, "statsfile_" + new System.Random().Next() + ".log");
            args = $"-y -i \"{pathToFile}\" -filter:v \"crop={cropParams.cropWidth}:{cropParams.cropHeight}:{cropParams.cropOriginX}:{cropParams.cropOriginY}, showinfo=n\" {forceEightMegStr} -vcodec libx264 -pass 1 -an -loglevel info -strict -2 -passlogfile {passLogFileLoc} -f null NUL";
        }
        else {
            args = $"-i \"{pathToFile}\" -filter:v \"crop={cropParams.cropWidth}:{cropParams.cropHeight}:{cropParams.cropOriginX}:{cropParams.cropOriginY}, showinfo=n\" -loglevel info \"{outPath}\"";
        }

        var proc = ProcessRunningUtils.RunExe(FFMPEGPath, new[] { args }, out string ignore, out string ignore2, false, onErrorDataReceived: OnFFMpegOut);

        if (forceEightMeg) {
            Action sec = () => {
                args = $"-i \"{pathToFile}\" -filter:v \"crop={cropParams.cropWidth}:{cropParams.cropHeight}:{cropParams.cropOriginX}:{cropParams.cropOriginY}, showinfo=n\" {forceEightMegStr} -vcodec libx264 -pass 2 -loglevel info -strict -2 -passlogfile {passLogFileLoc} \"{outPath}\"";
                proc = ProcessRunningUtils.RunExe(FFMPEGPath, new[] { args }, out ignore, out ignore2, false, onErrorDataReceived: OnFFMpegOut);
                StartCoroutine(WaitForProcess(proc));
            };
            StartCoroutine(WaitForProcess(proc, sec));
        }*/

        var proc = ProcessRunningUtils.RunExe(FFMPEGPath, new[] {
            $"-i \"{pathToFile}\" -filter:v \"crop={cropParams.cropWidth}:{cropParams.cropHeight}:{cropParams.cropOriginX}:{cropParams.cropOriginY}, showinfo=n\" -loglevel info \"{outPath}\""
            },
            out string ignore, out string ignore2, false, onErrorDataReceived: OnFFMpegOut);
 
        StartCoroutine(WaitForProcess(proc));
    }

    private void Start() {
        SetVizStart();
        inProgressPanel.SetActive(false);
    }

    private void Update() {
        while(tofeedOnMain.Count != 0) {
            FeedLogViz(tofeedOnMain[0]);
            tofeedOnMain.RemoveAt(0);
        }

        //progressBar.fillAmount = lastObtainedFrame / (lastCropTotalLenghtSeconds * lastCropFPS);
        progressBar.fillAmount = lastObtainedTime / (lastCropTotalLenghtSeconds);        
    }

    private void SetVizStart() {
        inProgressPanel.SetActive(true);
        errorPanel.SetActive(false);
        doneBox.SetActive(false);
        logprg = -1;
        for (int i = 0; i < lastLogLineVizes.Length; i++) {
            FeedLogViz("");
        }
    }

    private IEnumerator WaitForProcess(System.Diagnostics.Process proc, Action onDone = null) {
        var stopwatch = System.Diagnostics.Stopwatch.StartNew();
        while(!proc.HasExited) {
            yield return null;
        }
        Debug.Log("FFMPEG is done, took "+stopwatch.Elapsed);

        doneBox.SetActive(true);

        System.Threading.Thread.Sleep(500);

        if(onDone != null) {
            onDone();
        }
    }

    List<string> tofeedOnMain = new List<string>();

    private void OnFFMpegOut(string logged) {
        Debug.Log("FFMPEG:" + logged);        

        tofeedOnMain.Add(logged);
        try {
            ProcessLog(logged);
        }
        catch(System.Exception e) {
            Debug.LogError("ProcessLog fail on " + logged+":\n"+e);
        }
    }

    [ContextMenu("est")]
    public void TesT() {
        ProcessLog("Duration: 00:01:47.52, start: 0.000000, bitrate: 4969 kb/s");
    }

    public float lastCropTotalLenghtSeconds = 10f;
    public float lastCropFPS = 10f;
    public float lastObtainedFrame = 0;
    public float lastObtainedTime = 0f;

    private void ProcessLog(string logged) {
        if(logged.Contains("Duration")) {
            var durIndx = logged.IndexOf("Duration: ")+ "Duration: ".Length;
            var tillPeriodInx = logged.IndexOf(", start");

            var timecode = logged.Substring(durIndx, tillPeriodInx - durIndx);
            Debug.Log(timecode);

            float seconds = 0f;
            var splits = timecode.Split(':');
            seconds += 3600 * float.Parse(splits[0]);
            seconds += 60 * float.Parse(splits[1]);

            var lastSplot = splits[2].Split('.');
            seconds += 1 * float.Parse(lastSplot[0]);
            seconds += 0.01f * float.Parse(lastSplot[1]);

            Debug.Log("SECS:" + seconds);
            lastCropTotalLenghtSeconds = seconds;
        }
        if(logged.Contains(" fps,")) {
            var fpsIndx = logged.IndexOf(" fps,");

            string numBuf = "";
            int numscanner = fpsIndx-1;
            while(true) {
                if (int.TryParse(logged[numscanner].ToString(), out int ignore)) {
                    numBuf = logged[numscanner] + numBuf;
                    numscanner--;
                }
                else break;
            }
            Debug.Log("FPS:" + numBuf);
            var fps = float.Parse(numBuf);
            lastCropFPS = fps;
        }
        if(logged.Contains("frame= ")) {
            var indx = logged.IndexOf("frame= ") + "frame= ".Length;

            string numBuf = "";
            int numscanner = indx;
            while (true) {
                if (logged[numscanner] == ' ') {
                    //do nothing
                }
                else if (int.TryParse(logged[numscanner].ToString(), out int ignore)) {
                    numBuf = numBuf + logged[numscanner];                    
                }
                else break;

                numscanner++;
            }
            Debug.Log("FRAME from debugloglevel:" + numBuf);
            lastObtainedFrame = float.Parse(numBuf);
        }
        if (logged.Contains(" n:")) {
            var indx = logged.IndexOf("n:") + "n:".Length;

            string numBuf = "";
            int numscanner = indx;
            while (true) {
                if (logged[numscanner] == ' ') {
                    //do nothing
                }
                else if (int.TryParse(logged[numscanner].ToString(), out int ignore)) {
                    numBuf = numBuf + logged[numscanner];
                }
                else break;

                numscanner++;
            }
            Debug.Log("FRAME from showinfo:" + numBuf);
            lastObtainedFrame = float.Parse(numBuf);
        }
        if (logged.Contains(" pts_time:")) {
            var indx = logged.IndexOf("pts_time:") + "pts_time:".Length;

            string numBuf = "";
            int numscanner = indx;
            while (true) {
                if (logged[numscanner] == ' ') {
                    //do nothing
                }
                else if (logged[numscanner] == '.' || int.TryParse(logged[numscanner].ToString(), out int ignore)) {
                    numBuf = numBuf + logged[numscanner];
                }
                else break;

                numscanner++;
            }
            Debug.Log("CURRTIME from showinfo:" + numBuf);
            lastObtainedTime = float.Parse(numBuf);
        }
    }

    int logprg = -1;    

    private void FeedLogViz(string logged) {
        logprg++;
        if(logprg == lastLogLineVizes.Length) {
            for (int i = 1; i < lastLogLineVizes.Length; i++) {
                lastLogLineVizes[i - 1].text = lastLogLineVizes[i].text;
            }
            logprg--;
        }
        lastLogLineVizes[logprg].text = logged;
    }
}
