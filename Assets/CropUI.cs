﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[System.Serializable]
public class CropParams
{
    public int cropOriginX;
    public int cropOriginY;
    public int cropWidth = 100;
    public int cropHeight = 100;
}

public class CropUI : MonoBehaviour
{
    public RawImage img;
    public VideoPlayer videoPlayer;

    uint clipWid = 500;
    uint clipHeig = 200;

    public CropParams cropParams;

    public RectTransform videoPlane;
    public RectTransform cropRectTrans;

    public Slider videoSlider;
    public Toggle forceEightMegToggle;

    // Update is called once per frame
    void Update()
    {
        videoPlane.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, clipWid);
        videoPlane.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, clipHeig);

        cropRectTrans.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, cropParams.cropWidth);
        cropRectTrans.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, cropParams.cropHeight);

        cropRectTrans.anchoredPosition = new Vector2(cropParams.cropOriginX, -cropParams.cropOriginY);

        if (Input.GetKey(KeyCode.RightArrow)) cropParams.cropOriginX += (int)(Time.deltaTime * 500);
        if (Input.GetKey(KeyCode.LeftArrow)) cropParams.cropOriginX -= (int)(Time.deltaTime * 500);
        if (Input.GetKey(KeyCode.UpArrow)) cropParams.cropOriginY -= (int)(Time.deltaTime * 500);
        if (Input.GetKey(KeyCode.DownArrow)) cropParams.cropOriginY += (int)(Time.deltaTime * 500);

        if (Input.GetKey(KeyCode.D)) cropParams.cropWidth += (int)(Time.deltaTime * 500);
        if (Input.GetKey(KeyCode.A)) cropParams.cropWidth -= (int)(Time.deltaTime * 500);
        if (Input.GetKey(KeyCode.W)) cropParams.cropHeight -= (int)(Time.deltaTime * 500);
        if (Input.GetKey(KeyCode.S)) cropParams.cropHeight += (int)(Time.deltaTime * 500);

        if (cropParams.cropOriginX < 0) cropParams.cropOriginX = 0;
        if (cropParams.cropOriginY < 0) cropParams.cropOriginY = 0;

        if (cropParams.cropHeight < 16) cropParams.cropHeight = 16;
        if (cropParams.cropWidth < 16) cropParams.cropWidth = 16;


        ignoreSlid = true;
        videoSlider.value = (float)videoPlayer.time;

        videoSlider.maxValue = (float)videoPlayer.length;
    }

    private void Start() {
        videoPlayer.errorReceived += VideoPlayer_errorReceived;
        videoSlider.onValueChanged.AddListener(SlidedSlider);
    }

    bool ignoreSlid = false;

    private void SlidedSlider(float arg0) {
        if(ignoreSlid) {
            ignoreSlid = false;
        }
        videoPlayer.time = arg0;
    }

    public void PressedPlayPause() {
        if (videoPlayer.isPlaying) videoPlayer.Pause();
        else videoPlayer.Play();
    }

    private void VideoPlayer_errorReceived(VideoPlayer source, string message) {
        Debug.Log(source + " " + message);
    }

    public void PressedOpenVideo() {
        var fill = UnityEditor.EditorUtility.OpenFilePanel("give vid to trim", @"\\MAX-LAPTOP2\Users\OWNER\Videos\Captures", "mp4");
        videoPlayer.url = fill;
        videoPlayer.Play();

        StartCoroutine(SetStartsWhenOpen());
    }

    private IEnumerator SetStartsWhenOpen() {
        while (true) {
            yield return null;

            if (videoPlayer.isPrepared)
                break;

            Debug.Log("loading vid");
            yield return null;
        }

        

        /*startPoint = 0f;
        endPoint = videoPlayer.clip.length;*/

        /*var vidLen = videoPlayer.frameCount / videoPlayer.frameRate;

        timeslider.maxValue = vidLen;
        timeslider.value = 0f;*/

        img.texture = videoPlayer.texture;

        clipWid = videoPlayer.width;
        clipHeig = videoPlayer.height;
    }

    public void PressedCrop() {
        FindObjectOfType<FFMPEGCropper>().RunCrop(videoPlayer.url, cropParams, forceEightMegToggle.isOn, (float)videoPlayer.length);
    }
}
